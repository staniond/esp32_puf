//
// Ondrej Stanicek
// staniond@fit.cvut.cz
// Czech Technical University - Faculty of Information Technology
// 2022
//
#include <stdio.h>
#include <string.h>
#include <soc/rtc.h>
#include <esp_sleep.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "image_data.h"
#include "puf_measurement.h"
#include "bit_array.h"
#include "nvs.h"
#include "ecc.h"
#include "esp32_puf.h"

#define PUF_RESPONSE_SLEEP_uS (10*1000)
#define PUFSLEEP_RESPONSE_SLEEP_uS (100000)
#define PUF_HW_THRESHOLD_PERCENT (48.5)
#define PUF_ERROR_THRESHOLD_PERCENT (0.15)

uint8_t* RTC_FAST_MEMORY = (uint8_t*) RTC_FAST_MEMORY_ADDRESS;
uint8_t* MEMORY_INITIALIZER;

uint8_t PUF_BUFFER[PUF_MEMORY_SIZE];


void power_down_sram() {
    CLEAR_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PU | RTC_CNTL_FASTMEM_FORCE_NOISO);
    SET_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PD | RTC_CNTL_FASTMEM_FORCE_ISO);
}

void power_up_sram() {
    CLEAR_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PD | RTC_CNTL_FASTMEM_FORCE_ISO);
    SET_PERI_REG_MASK(RTC_CNTL_PWC_REG, RTC_CNTL_FASTMEM_FORCE_PU | RTC_CNTL_FASTMEM_FORCE_NOISO);
}

void restore_sram(uint8_t *backup) {
    memcpy(RTC_FAST_MEMORY, backup, PUF_MEMORY_SIZE);
    free(backup);
}

uint8_t *backup_sram() {
    uint8_t *backup = malloc(PUF_MEMORY_SIZE);
    memcpy(backup, RTC_FAST_MEMORY, PUF_MEMORY_SIZE);
    return backup;
}

void turn_off_sram(int sleep_us) {
    power_down_sram();
    ets_delay_us(sleep_us); // busy loop
    power_up_sram();
    vTaskDelay(10 / portTICK_PERIOD_MS); // wait till sram really turns on and stabilizes (not necessary?)
}

void sram_reading(int sleep_us) {
    for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
        RTC_FAST_MEMORY[i] = MEMORY_INITIALIZER[i];
    }

    turn_off_sram(sleep_us);

    printf("%s", MEASUREMENT_TAG);
    for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
        printf("%02x ", RTC_FAST_MEMORY[i]);
    }
    printf("\n");
    printf("Turn off time: %duS\n", sleep_us);
}

void set_memory_init(int memory_init) {
    switch(memory_init) {
        case 0:
            MEMORY_INITIALIZER = zeros_memory;
            break;
        case 1:
            MEMORY_INITIALIZER = ones_memory;
            break;
        case 2:
            MEMORY_INITIALIZER = occasional_zero;
            break;
        case 3:
            MEMORY_INITIALIZER = occasional_one;
            break;
        case 4:
            MEMORY_INITIALIZER = cvut_memory;
            break;
        case 5:
            MEMORY_INITIALIZER = random_memory;
            break;
        default:
            MEMORY_INITIALIZER = zeros_memory;
            break;
    }
}

void print_sram_sleep(int sleep_us_start, int sleep_us_end, int step, int memory_init) {
    set_memory_init(memory_init);
    for (int sleep_us = sleep_us_start; sleep_us < sleep_us_end; sleep_us += step) {
        sram_reading(sleep_us);
    }
}

void sram_deep_sleep_reading(int sleep_us, bool first_iteration, bool last_iteration) {
    if(!first_iteration) {
        printf("%s", MEASUREMENT_TAG);
        for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
            printf("%02x ", PUF_BUFFER[i]);
        }
        printf("\n");
    }

    for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
        PUF_BUFFER[i] = MEMORY_INITIALIZER[i];
    }

    if(!last_iteration){
        esp_sleep_enable_timer_wakeup(sleep_us);
        esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
        esp_deep_sleep_start();
    }
}

void print_sram_deep_sleep(int sleep_us_start, int sleep_us_end, int memory_init, int current_sleep_us) {
    set_memory_init(memory_init);

    // TODO not stopping properly
    if (current_sleep_us <= sleep_us_end) {
        sram_deep_sleep_reading(current_sleep_us, sleep_us_start == current_sleep_us, sleep_us_end == current_sleep_us);
    }
}

void print_puf_response_raw(int sleep_us) {
    memset(RTC_FAST_MEMORY, 0x00, PUF_MEMORY_SIZE); // simulate someone is using the memory
    turn_off_sram(sleep_us);

    printf("%s", MEASUREMENT_TAG);
    for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
        printf("%02x ", RTC_FAST_MEMORY[i]);
    }
    printf("\n");
}

void print_puf_responses_raw(int iterations, int sleep_us) {
    for (int i = 0; i < iterations; ++i) {
        print_puf_response_raw(sleep_us);
    }
}

void print_pufsleep_response_raw(int sleep_us, bool first_iteration, bool last_iteration) {
    if(!first_iteration) {
        printf("%s", MEASUREMENT_TAG);
        for (int i = 0; i < PUF_MEMORY_SIZE; ++i) {
            printf("%02x ", PUF_BUFFER[i]);
        }
        printf("\n");
    }

    if(!last_iteration){
        esp_sleep_enable_timer_wakeup(sleep_us);
        esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
        esp_deep_sleep_start();
    }
}

void print_pufsleep_responses_raw(int iterations, int sleep_us, int iteration_progress) {
    if (iteration_progress <= iterations) {
        print_pufsleep_response_raw(sleep_us, iteration_progress == 0,
                                    iteration_progress == iterations);
    }
}

void print_puf_response(int sleep_us) {
    uint8_t * backup = backup_sram();

    // get stable bit mask
    uint8_t *mask;
    size_t mask_len;
    get_blob(&mask, &mask_len, PUF_MASK_KEY);
    assert(mask_len == PUF_MEMORY_SIZE);

    // get ecc data
    uint8_t *ecc_data;
    size_t ecc_len;
    get_blob(&ecc_data, &ecc_len, ECC_DATA_KEY);

    // measure PUF response and apply the mask
    memset(RTC_FAST_MEMORY, 0x00, PUF_MEMORY_SIZE); // simulate someone is using the memory
    turn_off_sram(sleep_us);

    uint8_t *masked_puf = malloc(ecc_len);
    apply_puf_mask(mask, ecc_len*8, RTC_FAST_MEMORY, PUF_MEMORY_SIZE, masked_puf, ecc_len);

    // correct the masked response using the ECC data
    uint8_t *result = malloc(ecc_len/8);
    correct_data(masked_puf, ecc_data, ecc_len, result, ecc_len/8);

    printf("%s", MEASUREMENT_TAG);
    for (int i = 0; i < ecc_len/8; ++i) {
        printf("%02x ", result[i]);
    }
    printf("\n");

    restore_sram(backup);
    free(ecc_data);
    free(mask);
    free(masked_puf);
    free(result);
}

void print_puf_responses(int iterations, int sleep_us) {
    for (int i = 0; i < iterations; ++i) {
        print_puf_response(sleep_us);
    }
}

void print_pufsleep_response(int sleep_us, bool first_iteration, bool last_iteration) {
    if(!first_iteration) {
        // get stable bit mask
        uint8_t *mask;
        size_t mask_len;
        get_blob(&mask, &mask_len, PUF_SLEEP_MASK_KEY);
        assert(mask_len == PUF_MEMORY_SIZE);

        // get ecc data
        uint8_t *ecc_data;
        size_t ecc_len;
        get_blob(&ecc_data, &ecc_len, ECC_SLEEP_DATA_KEY);

        uint8_t *masked_puf = malloc(ecc_len);
        apply_puf_mask(mask, ecc_len * 8, PUF_BUFFER, PUF_MEMORY_SIZE, masked_puf, ecc_len);

        // correct the masked response using the ECC data
        uint8_t *result = malloc(ecc_len / 8);
        correct_data(masked_puf, ecc_data, ecc_len, result, ecc_len / 8);

        printf("%s", MEASUREMENT_TAG);
        for (int i = 0; i < ecc_len / 8; ++i) {
            printf("%02x ", result[i]);
        }
        printf("\n");

        free(ecc_data);
        free(mask);
        free(masked_puf);
        free(result);
    }

    if(!last_iteration) {
        esp_sleep_enable_timer_wakeup(sleep_us);
        esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
        esp_deep_sleep_start();
    }
}

void print_pufsleep_responses(int iterations, int sleep_us, int iteration_progress) {
    if (iteration_progress <= iterations) {
        print_pufsleep_response(sleep_us, iteration_progress == 0, iteration_progress == iterations);
    }
}

void print_puf_response_both(int sleep_us) {
    uint8_t * backup = backup_sram();

    // get stable bit mask
    uint8_t *mask;
    size_t mask_len;
    get_blob(&mask, &mask_len, PUF_MASK_KEY);
    assert(mask_len == PUF_MEMORY_SIZE);

    // get ecc data
    uint8_t *ecc_data;
    size_t ecc_len;
    get_blob(&ecc_data, &ecc_len, ECC_DATA_KEY);

    // measure PUF response and apply the mask
    memset(RTC_FAST_MEMORY, 0x00, PUF_MEMORY_SIZE); // simulate someone is using the memory
    turn_off_sram(sleep_us);

    double puf_hw_percent = (double) 100 * hamming_weight(RTC_FAST_MEMORY, PUF_MEMORY_SIZE) / (PUF_MEMORY_SIZE * 8);

    uint8_t *masked_puf = malloc(ecc_len);
    apply_puf_mask(mask, ecc_len*8, RTC_FAST_MEMORY, PUF_MEMORY_SIZE, masked_puf, ecc_len);

    // correct the masked response using the ECC data
    uint8_t *result = malloc(ecc_len/8);
    double puf_errors_percent = (double) 100 * correct_data(masked_puf, ecc_data, ecc_len, result, ecc_len / 8) / (ecc_len * 8);

    restore_sram(backup); // need to restore sram before going to sleep (to restore code in RTC SRAM)

    printf("Errors: %lf, HW: %lf\n", puf_errors_percent, puf_hw_percent);
    if(puf_hw_percent < PUF_HW_THRESHOLD_PERCENT || puf_errors_percent > PUF_ERROR_THRESHOLD_PERCENT) {
        esp_sleep_enable_timer_wakeup(sleep_us);
        esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
        esp_deep_sleep_start();
    }

    printf("%s", MEASUREMENT_TAG);
    for (int i = 0; i < ecc_len/8; ++i) {
        printf("%02x ", result[i]);
    }
    printf("\n");

    command.puf_response.iteration_progress += 1;
    free(ecc_data);
    free(mask);
    free(masked_puf);
    free(result);
}

void print_puf_responses_both(int iterations, int sleep_us, int iteration_progress) {
    for (int i = iteration_progress; i <= iterations ; ++i) {
        print_puf_response_both(sleep_us);
    }
}

void print_pufsleep_responses_both(int iterations, int sleep_us, int iteration_progress) {
        // get stable bit mask
        uint8_t *mask;
        size_t mask_len;
        get_blob(&mask, &mask_len, PUF_SLEEP_MASK_KEY);
        assert(mask_len == PUF_MEMORY_SIZE);

        // get ecc data
        uint8_t *ecc_data;
        size_t ecc_len;
        get_blob(&ecc_data, &ecc_len, ECC_SLEEP_DATA_KEY);

        uint8_t *masked_puf = malloc(ecc_len);
        apply_puf_mask(mask, ecc_len * 8, PUF_BUFFER, PUF_MEMORY_SIZE, masked_puf, ecc_len);

        // correct the masked response using the ECC data
        uint8_t *result = malloc(ecc_len / 8);
        correct_data(masked_puf, ecc_data, ecc_len, result, ecc_len / 8);

        printf("%s", MEASUREMENT_TAG);
        for (int i = 0; i < ecc_len / 8; ++i) {
            printf("%02x ", result[i]);
        }
        printf("\n");

        free(ecc_data);
        free(mask);
        free(masked_puf);
        free(result);

        print_puf_responses_both(iterations, sleep_us, iteration_progress);
}

void get_puf_bit_frequency(uint16_t *puf_freq, const size_t len, const size_t measurements) {
    assert(len == PUF_MEMORY_SIZE * 8); // puf_freq is a frequency of each bit --> needs to be 8 times larger
    for (size_t i = 0; i < measurements; ++i) {
        turn_off_sram(PUF_RESPONSE_SLEEP_uS);
        for (int j = 0; j < len; ++j) {
            puf_freq[j] += array_getBit(RTC_FAST_MEMORY, PUF_MEMORY_SIZE, j);
        }
    }
}

void pufsleep_bit_frequency_helper(const size_t len, bool first_iteration, bool last_iteration) {
    uint16_t *puf_freq;
    if (first_iteration) {
        puf_freq = calloc(sizeof(uint16_t), len);
        set_blob((const uint8_t *) puf_freq, len * sizeof(uint16_t), PUF_FREQUENCY_KEY);

    } else {
        size_t byte_len;
        get_blob((uint8_t **) &puf_freq, &byte_len, PUF_FREQUENCY_KEY);
        assert(byte_len == len * sizeof(uint16_t));
        for (int j = 0; j < len; ++j) {
            puf_freq[j] += array_getBit(PUF_BUFFER, PUF_MEMORY_SIZE, j);
        }
        set_blob((const uint8_t *) puf_freq, len * sizeof(uint16_t), PUF_FREQUENCY_KEY);
        free(puf_freq);
    }

    if(!last_iteration){
        esp_sleep_enable_timer_wakeup(PUFSLEEP_RESPONSE_SLEEP_uS);
        esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
        esp_deep_sleep_start();
    }
}

void get_pufsleep_bit_frequency(uint16_t **puf_freq, const size_t len, const size_t measurements, const int iteration_progress) {
    assert(len == PUF_MEMORY_SIZE * 8); // puf_freq is a frequency of each bit --> needs to be 8 times larger

    if(iteration_progress <= measurements) {
        pufsleep_bit_frequency_helper(len, iteration_progress == 0, iteration_progress == measurements);
    }

    size_t freq_len;
    get_blob((uint8_t **) puf_freq, &freq_len, PUF_FREQUENCY_KEY);
    // TODO rewrite and erase from NVS

    assert(freq_len == PUF_MEMORY_SIZE * 8 * sizeof(uint16_t)); // one uint16_t for each puf response byte
}
