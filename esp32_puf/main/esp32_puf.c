//
// Ondrej Stanicek
// staniond@fit.cvut.cz
// Czech Technical University - Faculty of Information Technology
// 2022
//
#include <stdio.h>
#include <string.h>
#include <driver/uart.h>
#include <stdbool.h>
#include <stdnoreturn.h>
#include <esp_sleep.h>
#include "freertos/task.h"

#include "nvs.h"
#include "puf_measurement.h"
#include "ecc.h"
#include "wake_up_stub.h"
#include "esp32_puf.h"

#define UART_BAUD_RATE (115200)
#define LINE_BUFFER_SIZE (1024)
#define NAME_TAG "_____NAME_TAG_____\n"
#define READY_TAG "_____READY_____\n"
#define RTC_TICKS_TO_mS ((20.0/3)/1000)
#define uS_TO_S_FACTOR (1000000)

#define CMD_PROVISION "PROVISION"
#define CMD_RESTART "RESTART"
#define CMD_PUF_RESPONSE_RAW "PUF_RESPONSE_RAW"
#define CMD_PUFSLEEP_RESPONSE_RAW "PUFSLEEP_RESPONSE_RAW"
#define CMD_PUF_RESPONSE "PUF_RESPONSE"
#define CMD_PUF_RESPONSE_BOTH "PUF_RESPONSE_BOTH"
#define CMD_SRAM_SLEEP "SRAM_SLEEP"
#define CMD_SRAM_DEEPSLEEP "SRAM_DEEPSLEEP"
#define CMD_PUFSLEEP_RESPONSE "PUFSLEEP_RESPONSE"
#define CMD_PROVISION_SLEEP "PROVISION_SLEEP"
#define CMD_PROVISION_BOTH "PROVISION_BOTH"

Command command;

/**
 * Initializes the app - sets baud rate and blocking UART driver
 */
void init() {
    uart_config_t uart_config = {
            .baud_rate = UART_BAUD_RATE,
            .data_bits = UART_DATA_8_BITS,
            .parity    = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_APB,
    };
    // uart 0 is the one connected to USB-UART chip
    esp_err_t err = uart_driver_install(UART_NUM_0, 2*1024, 0, 0, NULL, 0);
    ESP_ERROR_CHECK(err);
    uart_param_config(UART_NUM_0, &uart_config);
    ESP_ERROR_CHECK(err);

    //bootloader UART output is hardcoded to 115200 bauds, so it is read as junk
    //so print a new line to separate the junk from our output
    printf("\n");
}

/**
 * Gets the device name from NVS and prints it to UART.
 * If no device name is set in NVS, prints a default value.
 * Also prints a ESP32 revision number.
 */
void print_device_info() {
    char* device_name = NULL;
    bool result = get_device_name(&device_name);

    printf("%s", NAME_TAG);
    if(result) {
        printf("%s\n", device_name);
    }else{
        printf("%s\n", "UNKNOWN");
    }

    free(device_name);

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("Chip revision: %d\n", chip_info.revision);
}

/**
 * Blocking function that reads one line from stdin (UART0) into the \p buffer.
 * Resulting string will be null-terminated without a line feed character.
 * If buffer_size <= 0 the buffer will not be touched and returns immediately.
 * @param buffer buffer to which write the line
 * @param buffer_size size of the buffer in bytes
 */
void read_line(char* buffer, size_t buffer_size) {
    if(buffer_size <= 0) return;
    char *char_ptr = buffer;
    char *last_ptr = buffer + buffer_size - 1; // last valid pointer

    while (char_ptr != last_ptr) {
        int size = uart_read_bytes(UART_NUM_0, char_ptr, 1, portMAX_DELAY);
        if (size == 1) {
            if (*char_ptr == '\r') break;
            char_ptr++;
        }
    }
    *char_ptr = '\0';
}

/**
 * Parses command and it's arguments from \p command_string.
 * @param command_string C string with the command
 * @param command pointer to a Command struct that will be filled with parsed data of the command
 * @return true if command was successfully parsed, false on syntax error
 */
bool parse_command(const char* command_string) {
    memset(&command, 0x00, sizeof(command));
    char command_name[100] = {0}; // !also limit in sscanf!
    int arg_pos = 0;

    int result = sscanf(command_string, "%99s%n", command_name, &arg_pos);
    if(result != 1) return false;

    if(strcmp(command_name, CMD_SRAM_SLEEP) == 0) {
        command.type = SRAM_SLEEP;
        result = sscanf(command_string + arg_pos, "%d %d %d %d",
                        &command.sram_sleep.sleep_us_start,
                        &command.sram_sleep.sleep_us_end,
                        &command.sram_sleep.step,
                        &command.sram_sleep.memory_init);
        if (result != 4) return false;

    }else if(strcmp(command_name, CMD_SRAM_DEEPSLEEP) == 0) {
        command.type = SRAM_DEEPSLEEP;
        result = sscanf(command_string+arg_pos, "%d %d %d %d",
                        &command.sram_sleep.sleep_us_start,
                        &command.sram_sleep.sleep_us_end,
                        &command.sram_sleep.step,
                        &command.sram_sleep.memory_init);
        if(result != 4) return false;

    }else if(strcmp(command_name, CMD_PUF_RESPONSE_RAW) == 0){
        command.type = PUF_RESPONSE_RAW;
        result = sscanf(command_string+arg_pos, "%d %d",
                        &command.puf_response.iterations,
                        &command.puf_response.sleep_us);
        if(result != 2) return false;

    }else if(strcmp(command_name, CMD_PUFSLEEP_RESPONSE_RAW) == 0){
        command.type = PUFSLEEP_RESPONSE_RAW;
        result = sscanf(command_string+arg_pos, "%d %d",
                        &command.puf_response.iterations,
                        &command.puf_response.sleep_us);
        if(result != 2) return false;

    }else if(strcmp(command_name, CMD_PUF_RESPONSE_BOTH) == 0){
        command.type = PUF_RESPONSE_BOTH;
        result = sscanf(command_string+arg_pos, "%d %d",
                        &command.puf_response.iterations,
                        &command.puf_response.sleep_us);
        if(result != 2) return false;

    }else if(strcmp(command_name, CMD_PUF_RESPONSE) == 0){
        command.type = PUF_RESPONSE;
        result = sscanf(command_string+arg_pos, "%d %d",
                        &command.puf_response.iterations,
                        &command.puf_response.sleep_us);
        if(result != 2) return false;

    }else if(strcmp(command_name, CMD_PUFSLEEP_RESPONSE) == 0){
        command.type = PUFSLEEP_RESPONSE;
        result = sscanf(command_string+arg_pos, "%d %d",
                        &command.puf_response.iterations,
                        &command.puf_response.sleep_us);
        if(result != 2) return false;

    }else if(strcmp(command_name, CMD_RESTART) == 0){
        command.type = RESTART;

    }else if(strcmp(command_name, CMD_PROVISION) == 0){
        command.type = PROVISION;

    }else if(strcmp(command_name, CMD_PROVISION_SLEEP) == 0){
        command.type = PROVISION_SLEEP;

    }else if(strcmp(command_name, CMD_PROVISION_BOTH) == 0){
        command.type = PROVISION_BOTH;

    }else{
        return false;
    }
    return true;
}

/**
 * Executes the given command.
 * @param command the command to execute
 */
void execute_command() {
    switch(command.type) {
        case PUF_RESPONSE:
            print_puf_responses(
                    command.puf_response.iterations,
                    command.puf_response.sleep_us);
            break;
        case PUFSLEEP_RESPONSE:
            command.puf_response.iteration_progress = 0;
            print_pufsleep_responses(
                    command.puf_response.iterations,
                    command.puf_response.sleep_us,
                    command.puf_response.iteration_progress);
            break;
        case PUF_RESPONSE_BOTH:
            command.puf_response.iteration_progress = 0;
            print_puf_responses_both(
                    command.puf_response.iterations,
                    command.puf_response.sleep_us,
                    command.puf_response.iteration_progress);
            break;
        case PUF_RESPONSE_RAW:
            print_puf_responses_raw(
                    command.puf_response.iterations,
                    command.puf_response.sleep_us);
            break;
        case PUFSLEEP_RESPONSE_RAW:
            command.puf_response.iteration_progress = 0;
            print_pufsleep_responses_raw(
                    command.puf_response.iterations,
                    command.puf_response.sleep_us,
                    command.puf_response.iteration_progress);
            break;
        case SRAM_SLEEP:
            print_sram_sleep(
                    command.sram_sleep.sleep_us_start,
                    command.sram_sleep.sleep_us_end,
                    command.sram_sleep.step,
                    command.sram_sleep.memory_init);
            break;
        case SRAM_DEEPSLEEP:
            print_sram_deep_sleep(
                    command.sram_sleep.sleep_us_start,
                    command.sram_sleep.sleep_us_end,
                    command.sram_sleep.memory_init,
                    command.sram_sleep.sleep_us_start);
            break;
        case PROVISION:
            provision_puf_rtc();
            break;
        case PROVISION_SLEEP:
            command.provision_sleep.iteration_progress = 0;
            provision_puf_sleep(command.provision_sleep.iteration_progress);
            break;
        case PROVISION_BOTH:
            command.provision_sleep.iteration_progress = 0;
            provision_puf_both(command.provision_sleep.iteration_progress);
            break;
        case RESTART:
            printf("RESTARTING\n");
            esp_restart();
    }
}

/**
 * Reads a line from UART, parses it into a command and executes in in a loop.
 * This function never returns.
 */
noreturn void main_loop() {
    char* line_buffer = malloc(LINE_BUFFER_SIZE);

    while(true) {
        printf(READY_TAG);
        read_line(line_buffer, LINE_BUFFER_SIZE);
        if(!parse_command(line_buffer)){
            printf("SYNTAX ERROR\n");
            continue;
        }
        execute_command();
    }
}

/**
 * Restores the state of the application after deep sleep using the Command struct
 * saved in the RTC SRAM memory.
 */
void restore_state(void) {
    esp_reset_reason_t rr = esp_reset_reason();
    if(rr != ESP_RST_DEEPSLEEP) return;
    printf("Sleep time: %lfms\n", (then - now) * RTC_TICKS_TO_mS * 1000);

    if(command.type == PUFSLEEP_RESPONSE_RAW) {
        command.puf_response.iteration_progress += 1;
        print_pufsleep_responses_raw(command.puf_response.iterations,
                                     command.puf_response.sleep_us,
                                     command.puf_response.iteration_progress);
    }else if(command.type == SRAM_DEEPSLEEP) {
        command.sram_sleep.current_sleep_us += command.sram_sleep.step;
        print_sram_deep_sleep(command.sram_sleep.sleep_us_start,
                              command.sram_sleep.sleep_us_end,
                              command.sram_sleep.memory_init,
                              command.sram_sleep.current_sleep_us);
    }else if(command.type == PROVISION_SLEEP) {
        command.provision_sleep.iteration_progress += 1;
        printf("PROVISION SLEEP ITERATION %d\n", command.provision_sleep.iteration_progress);
        provision_puf_sleep(command.provision_sleep.iteration_progress);
    }else if(command.type == PROVISION_BOTH) {
        command.provision_sleep.iteration_progress += 1;
        printf("PROVISION SLEEP ITERATION %d\n", command.provision_sleep.iteration_progress);
        provision_puf_both(command.provision_sleep.iteration_progress);
    }else if(command.type == PUFSLEEP_RESPONSE) {
        command.puf_response.iteration_progress += 1;
        print_pufsleep_responses(command.puf_response.iterations,
                                 command.puf_response.sleep_us,
                                 command.puf_response.iteration_progress);
    }else if(command.type == PUF_RESPONSE_BOTH) {
        command.puf_response.iteration_progress += 1;
        print_pufsleep_responses_both(command.puf_response.iterations,
                                      command.puf_response.sleep_us,
                                      command.puf_response.iteration_progress);
    }
}

void app_main(void) {
    init();
    print_device_info();
    restore_state();
    main_loop();
}
