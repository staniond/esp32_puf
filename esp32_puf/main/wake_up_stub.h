//
// Ondrej Stanicek
// staniond@fit.cvut.cz
// Czech Technical University - Faculty of Information Technology
// 2022
//
#ifndef ESP32_PUF_WAKE_UP_STUB_H
#define ESP32_PUF_WAKE_UP_STUB_H

extern int RTC_DATA_ATTR wake_up_counter;

/**
 * A version of the ESP-IDF rtc_time_get function usable from the deep sleep wake stub.
 * @return RTC timer clock cycles since the start
 */
uint64_t RTC_IRAM_ATTR rtc_time_get_own(void);

extern uint64_t RTC_DATA_ATTR now;
extern uint64_t RTC_DATA_ATTR then;

#endif //ESP32_PUF_WAKE_UP_STUB_H
