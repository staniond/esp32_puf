//
// Ondrej Stanicek
// staniond@fit.cvut.cz
// Czech Technical University - Faculty of Information Technology
// 2022
//
#ifndef ESP32_PUF_PUF_MEASUREMENT_H
#define ESP32_PUF_PUF_MEASUREMENT_H

#include <esp_attr.h>

#define PUF_MEMORY_SIZE 0x1000 // max is 0x2000 - 8KB for RTC FAST SRAM
#define RTC_FAST_MEMORY_ADDRESS (0x3FF80000)
#define DATA_SRAM_MEMORY_ADDRESS (0x3FFB0000)

#define MEASUREMENT_TAG "_____MEASUREMENT_TAG_____\n"

/**
 * Deep sleep SRAM data is copied here after wakeup by the deep sleep wake stub. The PUF response
 * is then reconstructed from the raw data in this buffer.
 */
extern __NOINIT_ATTR uint8_t PUF_BUFFER[PUF_MEMORY_SIZE];

/**
 * Powers down the RTC fast memory 8kB SRAM
 */
void power_down_sram();

/**
 * Powers up the RTC fast memory 8kB SRAM
 */
void power_up_sram();

/**
 * Backs up the contents of RTC fast memory to a buffer.
 * @return the buffer with the RTC fast memory backup
 */
uint8_t* backup_sram();

/**
 * Restores the RTC fast memory backup.
 * @param backup pass a pointer that was returned by the backup_sram function
 */
void restore_sram(uint8_t* backup);

/**
 * Powers the RTC fast memory down for \p sleep_us microseconds and then powers it up again.
 * @param sleep_us number of microseconds to leave the SRAM off
 */
void turn_off_sram(int sleep_us);

/**
 * Initializes SRAM, then turns it off for specified amount of time, reads it and sends the data to UART.
 * @param sleep_us time the SRAM stays turned off in microseconds
 */
void sram_reading(int sleep_us);

/**
 *
 * Initializes SRAM, then turns it off for specified amount of time, reads it and sends the data to UART.
 * Deep sleep PUF version is used.
 * @param sleep_us time the SRAM stays turned off in microseconds
 * @param first_iteration true if this is the first iteration (should not print PUF response before going to sleep)
 * @param last_iteration true if this is the last iteration (should not go to sleep)
 */
void sram_deep_sleep_reading(int sleep_us, bool first_iteration, bool last_iteration);

/**
 * Sets the initializer which will fill the memory during sram_sleep measurements.
 * memory init values:
 * 0 - 0x00 bytes
 * 1 - 0xff bytes
 * 2 - cvut image
 * 3 - random byte values
 * otherwise - 0x00 bytes
 * @param memory_init
 */
void set_memory_init(int memory_init);

/**
 * Performs SRAM measurements using \f sram_reading in a loop -
 * starting at \p sleep_us_start SRAM turn off time,incrementing by \p step
 * until \p sleep_us_end and sends each measurement to UART.
 * @param sleep_us_start start time of measurements in microseconds
 * @param sleep_us_end stop time of measurements in microseconds
 * @param step turn off time increment in microseconds
 * @param memory_init memory initializer number according to set_memory_init function
 */
void print_sram_sleep(int sleep_us_start, int sleep_us_end, int step, int memory_init);

/**
 *
 */
/**
 * Performs SRAM measurements using \f sram_reading in a loop -
 * starting at \p sleep_us_start SRAM turn off time,incrementing by \p step
 * until \p sleep_us_end and sends each measurement to UART.
 * Deep sleep PUF version is used.
 * @param sleep_us_start start time of measurements in microseconds
 * @param sleep_us_end stop time of measurements in microseconds
 * @param current_sleep_us the current sleep time
 * @param memory_init memory initializer number according to set_memory_init function
 */
void print_sram_deep_sleep(int sleep_us_start, int sleep_us_end, int memory_init, int current_sleep_us);

/**
 * Reads a SRAM PUF response and sends it to UART.
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 */
void print_puf_response_raw(int sleep_us);

/**
 * Sends \p iterations of PUF responses to UART.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 */
void print_puf_responses_raw(int iterations, int sleep_us);

/**
 * Reads a SRAM PUF response and sends it to UART.
 * Deep sleep PUF version is used.
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param first_iteration true if this is the first iteration (should not print PUF response before going to sleep)
 * @param last_iteration true if this is the last iteration (should not go to sleep)
 */
void print_pufsleep_response_raw(int sleep_us, bool first_iteration, bool last_iteration);

/**
 * Sends \p iterations of PUF responses to UART.
 * Deep sleep PUF version is used.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param iteration_progress which iteration should be now executed
 */
void print_pufsleep_responses_raw(int iterations, int sleep_us, int iteration_progress);

/**
 * Reads a SRAM PUF response, corrects it using a saved mask and ECC helper data and sends it to UART.
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 */
void print_puf_response(int sleep_us);

/**
 * Sends \p iterations of ECC corrected PUF responses to UART.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 */
void print_puf_responses(int iterations, int sleep_us);

/**
 * Reads a SRAM PUF response, corrects it using a saved mask and ECC helper data and sends it to UART.
 * Deep sleep PUF version is used.
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param first_iteration true if this is the first iteration (should not print PUF response before going to sleep)
 * @param last_iteration true if this is the last iteration (should not go to sleep)
 */
void print_pufsleep_response(int sleep_us, bool first_iteration, bool last_iteration);

/**
 * Sends \p iterations of ECC corrected PUF responses to UART.
 * Deep sleep PUF version is used.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param iteration_progress which iteration should be now executed
 */
void print_pufsleep_responses(int iterations, int sleep_us, int iteration_progress);

/**
 * Sends \p iterations of ECC corrected PUF responses to UART.
 * Determines if RTC sram can be used. If not, uses deepsleep version instead.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param iteration_progress which iteration should be now executed
 */
void print_puf_responses_both(int iterations, int sleep_us, int iteration_progress);

/**
 * Helper function which needs to be called while PUF_RESPONSE_BOTH command is used when restoring
 * state after deep sleep to resume operation properly.
 * Determines if RTC sram can be used. If not, uses deepsleep version instead.
 * @param iterations how many PUF responses to take
 * @param sleep_us number of microseconds to leave SRAM off before taking a measurement
 * @param iteration_progress which iteration should be now executed
 */
void print_pufsleep_responses_both(int iterations, int sleep_us, int iteration_progress);

/**
 * Generates frequency array of the PUF response.
 * puf_freq[i] is the number of times the i-th bit in the puf response was 1 during \p measurements measurements.
 * (if the bit is 1 all the time, puf_freq[i] == \p measurements)
 * @param puf_freq the resulting frequency array
 * @param len length of the \p puf_freq (needs to be 8 * size of the PUF SRAM region read)
 * @param measurements number of PUF measurements to take
 */
void get_puf_bit_frequency(uint16_t *puf_freq, size_t len, size_t measurements);

/**
 * Generates frequency array of the PUF response.
 * puf_freq[i] is the number of times the i-th bit in the puf response was 1 during \p measurements measurements.
 * (if the bit is 1 all the time, puf_freq[i] == \p measurements)
 * Deep sleep PUF version is used.
 * @param puf_freq the resulting frequency array will be set to this buffer
 * @param len length of the \p puf_freq (needs to be 8 * size of the PUF SRAM region read)
 * @param measurements number of PUF measurements to take
 * @param iteration_progress which iteration should be now executed
 */
void get_pufsleep_bit_frequency(uint16_t **puf_freq, size_t len, size_t measurements, int iteration_progress);

#endif //ESP32_PUF_PUF_MEASUREMENT_H
