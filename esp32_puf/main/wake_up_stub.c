//
// Ondrej Stanicek
// staniond@fit.cvut.cz
// Czech Technical University - Faculty of Information Technology
// 2022
//
#include <stdio.h>
#include <esp_sleep.h>
#include <string.h>
#include <esp_system.h>
#include <soc/rtc_cntl_reg.h>

#include "wake_up_stub.h"
#include "puf_measurement.h"

int wake_up_counter = 0;
uint64_t now = 0;
uint64_t then = 0;

uint64_t rtc_time_get_own(void) {
    SET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_UPDATE);
    while (GET_PERI_REG_MASK(RTC_CNTL_TIME_UPDATE_REG, RTC_CNTL_TIME_VALID) == 0) {
    }
    SET_PERI_REG_MASK(RTC_CNTL_INT_CLR_REG, RTC_CNTL_TIME_VALID_INT_CLR);
    uint64_t t = READ_PERI_REG(RTC_CNTL_TIME0_REG);
    t |= ((uint64_t) READ_PERI_REG(RTC_CNTL_TIME1_REG)) << 32;
    return t;
}

void RTC_IRAM_ATTR esp_wake_deep_sleep(void) {
    then = rtc_time_get_own();
    esp_default_wake_deep_sleep();
    wake_up_counter++;
    memcpy(PUF_BUFFER, (uint8_t*) DATA_SRAM_MEMORY_ADDRESS, PUF_MEMORY_SIZE);
}

