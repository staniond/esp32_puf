//
// Created by standa on 3/14/22.
//

#ifndef ESP32_PUF_ESP32_PUF_H
#define ESP32_PUF_ESP32_PUF_H

#include <esp_attr.h>

/**
 * This struct represents a command currently executed by the application. It is used to restore state correctly
 * after deep sleep wake up.
 */
typedef struct {
    enum command_type {RESTART, PUF_RESPONSE_RAW, SRAM_SLEEP, PUF_RESPONSE, PUF_RESPONSE_BOTH, PROVISION, PROVISION_BOTH,
        PUFSLEEP_RESPONSE_RAW, SRAM_DEEPSLEEP, PUFSLEEP_RESPONSE, PROVISION_SLEEP} type;
    // arguments of given command:
    union {
        struct {
            int iterations;
            int sleep_us;
            int iteration_progress;
        } puf_response;
        struct {
            int sleep_us_start;
            int sleep_us_end;
            int step;
            int memory_init;
            int current_sleep_us;
        } sram_sleep;
        struct {
            int iteration_progress;
        } provision_sleep;
    };
} Command;

extern Command RTC_DATA_ATTR command;

#endif //ESP32_PUF_ESP32_PUF_H
