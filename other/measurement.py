#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
#
from esp_commander import start_measurement, ESPCommander


def commands(commander: ESPCommander) -> None:
    temps = ((-40, "minus_40/"), (-30, "minus_30/"), (-20, "minus_20/"), (-10, "minus_10/"), (0, "plus_0/"),(10, "plus_10/"))

    for temp, subdir in temps:
        commander.set_output_subdir(subdir)
        commander.set_temperature(temp)

        commander.puf_response_both(1000, 100_000)
        commander.reset_board()
        commander.puf_response_raw(1000, 100_000)
        commander.reset_board()
        commander.pufsleep_response_raw(1000, 100_000)
        commander.reset_board()
        commander.sram_sleep(0, 1000, 5, 0)
        commander.reset_board()
        commander.sram_sleep(0, 50_000, 25, 0)
        commander.reset_board()
        commander.sram_deepsleep(0, 100_000, 50, 0)
        commander.reset_board()
        commander.sram_deepsleep(0, 1000, 5, 0)
        commander.reset_board()
        
    commander.set_temperature(25)


def main() -> None:
    start_measurement(commands, output_dir=None, log_file=None, debug=True)


if __name__ == '__main__':
    main()
