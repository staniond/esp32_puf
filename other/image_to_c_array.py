#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
#
import sys
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


# returns np array image, each byte represents 8 pixels (1 bit == 1 pixel)
# the array is has 4069 bytes, the image shape is (128, 256)
def get_image_bytes(image_path):
    img = Image.open(image_path).resize((256,128)).convert('L')
    pixels =  np.asarray(img)
    return np.packbits(pixels//255)


#prints the np array image as a valid c array initializer - needs uint8_t name[4096] array
def print_c_array(memory_values):
    print("{")
    for i in range(memory_values.size):
        print("0x{:02x}".format(memory_values[i]), end=', ')
        if i % 32 == 0:
            print()
    print("};")


if len(sys.argv) != 2:
    print("Usage:")
    print("\timage_to_c_array.py path_to_image")
    exit(1)

memory_values = get_image_bytes(sys.argv[1])
print_c_array(memory_values)

