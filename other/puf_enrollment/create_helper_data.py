#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
#
import numpy as np
import argparse
import os


def load_data(path, bits=True):
    data = np.fromfile(path, dtype='uint8')
    if bits:
        data = np.unpackbits(data, bitorder="little")
    return data


def get_reference_response(data):
    mean = data.mean(0)
    ref = np.where(mean > 0.5, 1, 0)
    return ref


def get_data_from_files(paths):
    rtc_data = []
    main_data = []
    for path in paths:
        data = load_data(path, bits=True)
        data = data.reshape((-1, 4096 * 8))
        half = data.shape[0] // 2
        rtc_data.append(data[:half])
        main_data.append(data[half:])

    all_rtc_data = np.concatenate(rtc_data, axis=0)
    all_main_data = np.concatenate(main_data, axis=0)
    return all_rtc_data, all_main_data


def get_mask(puf_responses):
    mean = puf_responses.mean(0)
    stable_ones = np.where(mean == 1, 1, 0).astype(np.uint8)
    stable_zeros = np.where(mean == 0, 1, 0).astype(np.uint8)
    return stable_ones | stable_zeros


def trim_mask(mask, hw):
    hw = hw - hw % 64
    counter = 0
    trimmed_mask = []
    for bit in mask:
        if counter < hw:
            trimmed_mask.append(bit)
        else:
            trimmed_mask.append(0)
        if bit:
            counter += 1
    return np.asarray(trimmed_mask, dtype="uint8")


def create_mask(puf_responses):
    mean = puf_responses.mean(0)
    stable_ones = np.where(mean == 1, 1, 0).astype(np.uint8)
    stable_zeros = np.where(mean == 0, 1, 0).astype(np.uint8)
    return stable_ones | stable_zeros


def apply_mask(data, mask):
    not_mask = np.logical_not(mask)
    masked = np.ma.masked_array(data, mask=not_mask)
    return np.packbits(masked.compressed(), bitorder="little")


def apply_mask2(data, mask):
    result = []
    for i in range(len(mask)):
        if mask[i] == 1:
            result.append(data[i])
    return np.packbits(np.asarray(result, dtype="uint8"), bitorder='little')


def create_ecc_data(masked_ref):
    ecc_data = []
    for byte in masked_ref:
        bit_set = (byte >> 7) & 1
        if bit_set:
            ecc_data.append(~byte)
        else:
            ecc_data.append(byte)
    return np.asarray(ecc_data, dtype="uint8")


def create_ecc_data_template(masked_ref, template):
    ecc_data = []
    for i in range(masked_ref.shape[0]):
        bit_set = (template[i] >> 7) & 1
        if bit_set:
            ecc_data.append(~masked_ref[i])
        else:
            ecc_data.append(masked_ref[i])
    return np.asarray(ecc_data, dtype="uint8")


def generate_nvs_csv(args):
    with open(args.output_dir + "/nvs.csv", "w") as file:
        file.write("key,type,encoding,value\n")
        file.write("storage,namespace,,\n")
        file.write(f"DEVICE_NAME,data,string,UNKNOWN\n")
        file.write(f"ECC_DATA,file,binary,{args.output_dir}/rtc_ecc_data.bin\n")
        file.write(f"ECC_SLEEP_DATA,file,binary,{args.output_dir}/main_ecc_data.bin\n")
        file.write(f"PUF_MASK,file,binary,{args.output_dir}/rtc_mask.bin\n")
        file.write(f"PUF_SLEEP_MASK,file,binary,{args.output_dir}/main_mask.bin\n")


def process_data(args):
    # parse the data files
    rtc_data, main_data = get_data_from_files(args.input_files)

    # create reference responses
    rtc_ref = get_reference_response(rtc_data)
    main_ref = get_reference_response(main_data)

    # create masks and trim them to the same size
    rtc_mask = create_mask(rtc_data)
    main_mask = create_mask(main_data)
    hw = min(rtc_mask.sum(), main_mask.sum())

    rtc_mask = trim_mask(rtc_mask, hw)
    main_mask = trim_mask(main_mask, hw)

    # apply masks to ref responses
    rtc_masked = apply_mask(rtc_ref, rtc_mask)
    main_masked = apply_mask(main_ref, main_mask)

    # generate ECC data
    main_ecc_data = create_ecc_data(main_masked)
    rtc_ecc_data = create_ecc_data_template(rtc_masked, main_masked)

    packed_rtc_mask = np.packbits(rtc_mask, bitorder='little')
    packed_main_mask = np.packbits(main_mask, bitorder='little')

    os.makedirs(args.output_dir, exist_ok=True)
    rtc_ecc_data.tofile(args.output_dir + "/rtc_ecc_data.bin")
    main_ecc_data.tofile(args.output_dir + "/main_ecc_data.bin")
    packed_rtc_mask.tofile(args.output_dir + "/rtc_mask.bin")
    packed_main_mask.tofile(args.output_dir + "/main_mask.bin")


def main(args):
    process_data(args)
    print("Generated helper PUF data")
    generate_nvs_csv(args)
    print("Generated NVS partition csv file")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("output_dir", type=str,
                        help="directory to which the helper data and .csv file with NVS partition is stored")
    parser.add_argument("input_files", help="paths to files with enrollment data", nargs="*", type=str)
    main(parser.parse_args())
