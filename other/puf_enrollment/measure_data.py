#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
#
import tqdm
import serial
import time
import argparse

MEASUREMENT_TAG = b'_____MEASUREMENT_TAG_____\r\n'


def sram_line_string_to_bytes(sram_line):
    stripped = sram_line.decode().strip()
    raw_bytes = bytes.fromhex(stripped)
    return raw_bytes


def wait_ready(ser):
    while line := ser.readline():
        if line == b"_____READY_____\r\n":
            break


def measure_data(measurements, command, ser):
    data = []
    time.sleep(1)
    ser.write(command.encode())
    i = 0
    with tqdm.tqdm(total=measurements) as pbar:
        while True:
            if i == measurements:
                break

            line = ser.readline()
            if line == MEASUREMENT_TAG:
                data.append(sram_line_string_to_bytes(ser.readline()))
                i += 1
                pbar.update(1)
    wait_ready(ser)
    return data


def main(args):
    command_puf = f"PUF_RESPONSE_RAW {args.measurements+5} 100000\r"  # command to get the RTC SRAM data
    command_pufsleep = f"PUFSLEEP_RESPONSE_RAW {args.measurements+5} 100000\r"  # command to get the main SRAM data

    with serial.Serial(args.port, args.baud_rate, timeout=10) as ser:
        print("Measuring RTC SRAM:")
        puf_data = measure_data(args.measurements, command_puf, ser)

        ser.write("RESTART\r".encode())
        wait_ready(ser)

        print("Measuring DATA SRAM:")
        pufsleep_data = measure_data(args.measurements, command_pufsleep, ser)

    result_data = puf_data + pufsleep_data
    with open(args.file, "wb") as file:
        for measurement in result_data:
            file.write(measurement)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="path to a file to save the data to", type=str)
    parser.add_argument("port", help="serial port to which the device is connected", type=str)
    parser.add_argument("-b", "--baud-rate", help="baud rate, default=115200", type=int, default=115200)
    parser.add_argument("-m", "--measurements", help="how many measurements of the SRAM data to take, default=1000",
                        type=int, default=1000)
    main(parser.parse_args())
