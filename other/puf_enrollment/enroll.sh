#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
#
if [ -z ${IDF_PATH+x} ];
then
  echo "set the IDF_PATH variable to path to esp-idf";
  exit 1
fi

if [ $# -eq 0 ]
  then
    echo "Help: enroll.sh port [measurements] [baud_rate]"
    printf "\tport:\tserial port of the device to enroll\n"
    printf "\tmeasurements:\tnumber of measurements to calculate the stable bit mask from (1000 default)\n"
    printf "\tbaud_rate:\tbaud rate for the serial interface (115200 default)\n"
    exit
fi

measurements=${2:-1000}
baud_rate=${3:-115200}

# needs to be exported for the python scripts
export IDF_PATH="$IDF_PATH"

TEMP_DIR="$(mktemp -d)"

# measure the raw PUF responses
python3 measure_data.py -m  "$measurements" "$TEMP_DIR"/enrollment_data.bin "$1" -b "$baud_rate"

# create helper data from the raw PUF data
python3 create_helper_data.py "$TEMP_DIR" "$TEMP_DIR"/enrollment_data.bin

source $IDF_PATH/export.sh

# generate the NVS partition binary from the data
python3 "$IDF_PATH"/components/nvs_flash/nvs_partition_generator/nvs_partition_gen.py generate "$TEMP_DIR"/nvs.csv "$TEMP_DIR"/partition.bin 0x10000

# flash the NVS partition to the device
"$IDF_PATH"/components/partition_table/parttool.py --port "$1" write_partition --partition-name=nvs --input="$TEMP_DIR"/partition.bin

echo 'Enrollment DONE'
