# original authors: Kryštof Šádek, Jiří Buček, Filip Kodýtek, 
# modified by: Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022
import logging
import socket
import struct
import time


class Chamber:
    BUFFER_SIZE = 1024
    READ_CURRENT_TEMP = [0x01, 0x04, 0x10, 0x04, 0x00, 0x02]
    READ_SENSOR_TEMP = [0x01, 0x04, 0x10, 0x0A, 0x00, 0x02]
    READ_SETPOINT_TEMP = [0x01, 0x04, 0x10, 0xb2, 0x00, 0x02]
    WRITE_SETPOINT_TEMP = [0x01, 0x10, 0x11, 0x4c, 0x00, 0x02, 0x04]
    WRITE_CONTROL_REG = [0x01, 0x10, 0x11, 0x58, 0x00, 0x01, 0x02]
    READ_CONTROL_REG = [0x01, 0x04, 0x12, 0x92, 0x00, 0x01]

    logger: logging.Logger

    def __init__(self, ip='10.11.58.21', port=10001, debug=False):
        self.debug = debug
        self.ip = ip
        self.port = port
        self.logger = logging.getLogger("chamber")

    @staticmethod
    def crc(data):
        crc = 0xffff
        polynomial = 0xa001

        for b in data:
            crc = crc ^ b
            for i in range(0, 8):
                if (crc & 1) == 1:
                    crc >>= 1
                    crc = crc ^ polynomial
                else:
                    crc >>= 1
        return crc

    def send(self, data) -> bytes:
        crc = self.crc(data)
        data = data + bytearray([crc & 0xff, crc >> 8])

        if not self.debug:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.ip, self.port))
            s.settimeout(5.0)
            s.send(data)
            time.sleep(1)
            recv_data = s.recv(self.BUFFER_SIZE)
            s.close()
            return recv_data
        else:
            self.logger.debug("Sending: {data}".format(data=data))
            time.sleep(1)
            return bytes([0x01, 0x03, 0x04, 0x00, 0x00, 0x41, 0xf0, 0, 0])

    def read_current_temp(self) -> float:
        recv_data = self.send(bytearray(self.READ_CURRENT_TEMP))
        return struct.unpack('>f', (recv_data[5:7] + recv_data[3:5]))[0]

    def read_sensor_temp(self) -> float:
        recv_data = self.send(bytearray(self.READ_SENSOR_TEMP))
        return struct.unpack('>f', (recv_data[5:7] + recv_data[3:5]))[0]

    def read_setpoint_temp(self) -> float:
        recv_data = self.send(bytearray(self.READ_SETPOINT_TEMP))
        return struct.unpack('>f', (recv_data[5:7] + recv_data[3:5]))[0]

    def write_setpoint_temp(self, temp) -> None:
        self.logger.info(f"Setting setpoint to {temp} degrees")
        temp_bytes = struct.pack('>f', temp)
        temp_modbus = temp_bytes[2:4] + temp_bytes[0:2]

        data = bytearray(self.WRITE_SETPOINT_TEMP)
        data = data + temp_modbus
        self.send(data)

    def write_control_reg(self, register) -> None:
        bytes = struct.pack('>H', register)
        data = bytearray(self.WRITE_CONTROL_REG)
        data = data + bytes
        self.send(data)

    def read_control_reg(self) -> int:
        recv_data = self.send(bytearray(self.READ_CONTROL_REG))
        return struct.unpack('>H', (recv_data[3:5]))[0]

    def clear_idle(self) -> None:
        self.logger.info("Clearing idle")
        register = self.read_control_reg()
        if register & 2 != 0:
            self.write_control_reg(register & ~2)

    def set_idle(self) -> None:
        self.logger.info("Setting idle")
        register = self.read_control_reg()
        if register & 2 == 0:
            self.write_control_reg(register | 2)

    def is_idle(self) -> bool:
        register = self.read_control_reg()
        return register & 2 != 0

