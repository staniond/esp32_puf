#
# Ondrej Stanicek
# staniond@fit.cvut.cz
# Czech Technical University - Faculty of Information Technology
# 2022

#
# Logging solution inspired by:
# https://docs.python.org/3/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
#
# Faster serial.readline implementation taken from: https://github.com/pyserial/pyserial/issues/216
#

import time
import serial
import serial.tools.list_ports
import sys
import os
import logging
import logging.handlers
from typing import Optional, Callable
from multiprocessing import Process, Queue, JoinableQueue
from dataclasses import dataclass
from esp_commander.chamber import Chamber

MEASUREMENT_SIZE = 0x1000
LOG_LEVEL = logging.INFO
LOG_FORMAT_STDOUT = '%(asctime)s  %(name)-8s %(levelname)-8s%(message)s'
LOG_FORMAT_LOG = '%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s'
TSV_HEADER = 'timestamp\tprocess\tlevel\tmessage\n'


class CustomSerial(serial.Serial):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.buf = bytearray()

    def readline(self, **kwargs) -> bytearray:
        i = self.buf.find(b"\n")
        if i >= 0:
            result = self.buf[:i + 1]
            self.buf = self.buf[i + 1:]
            return result
        while True:
            i = max(1, min(8096, self.in_waiting))
            data = self.read(i)
            i = data.find(b"\n")
            if i >= 0:
                result = self.buf + data[:i + 1]
                self.buf[0:] = data[i + 1:]
                return result
            else:
                self.buf.extend(data)


@dataclass
class Command:
    name: str
    kwargs: dict


class ESPBoard:
    baud_rate: int = 921600

    logger: logging.Logger
    serial: CustomSerial
    board_name: Optional[str]
    output_dir: str
    __file_suffix: str
    __subdir: str

    def __init__(self, uart_device: str, output_dir="data/"):
        self.logger = logging.getLogger(uart_device)  # device name unknown until open is called
        self.serial = CustomSerial(baudrate=self.baud_rate)
        self.serial.port = uart_device
        self.board_name = None
        self.output_dir = output_dir
        if self.output_dir:
            os.makedirs(self.output_dir, exist_ok=True)
        self.__file_suffix = ""
        self.__subdir = ""

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.close()

    def open(self) -> None:
        self.serial.open()
        self.reset_board(wait=False)

        # get device name after reset
        while line := self.serial.readline():
            if line == b'_____NAME_TAG_____\r\n':
                name = self.serial.readline().decode().strip()
                self.logger = logging.getLogger(name)  # set correct logger name - the board name
                self.logger.info(f"Board name: '{name}'")
                self.board_name = name
                self.__wait_ready()
                break

    def close(self) -> None:
        self.serial.close()

    def set_file_suffix(self, file_suffix: str) -> None:
        if file_suffix:
            self.__file_suffix = "_" + file_suffix
        else:
            self.__file_suffix = ""

    def set_output_subdir(self, subdir: str) -> None:
        if not self.output_dir:
            return  # output to file disabled

        if subdir:
            self.__subdir = subdir
            os.makedirs(self.output_dir + self.__subdir, exist_ok=True)
        else:
            self.__subdir = ""

    def reset_board(self, wait: bool = True) -> None:
        command = "RESTART\r".encode()
        self.__send_command(command)
        if wait:
            self.__read_measurements(0, command)

    def provision_board(self) -> None:
        command = "PROVISION_BOTH\r".encode()
        self.__send_command(command)
        self.__read_measurements(0, command)

    def get_puf_response(self, iterations: int, sleep_us: int) -> bytearray:
        command = f"PUF_RESPONSE {iterations} {sleep_us}\r".encode()
        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_puf_response_both(self, iterations: int, sleep_us: int) -> bytearray:
        command = f"PUF_RESPONSE_BOTH {iterations} {sleep_us}\r".encode()
        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_puf_response_raw(self, iterations: int, sleep_us: int) -> bytearray:
        command = f"PUF_RESPONSE_RAW {iterations} {sleep_us}\r".encode()
        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_pufsleep_response(self, iterations: int, sleep_us: int) -> bytearray:
        command = f"PUFSLEEP_RESPONSE {iterations} {sleep_us}\r".encode()
        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_pufsleep_response_raw(self, iterations: int, sleep_us: int) -> bytearray:
        command = f"PUFSLEEP_RESPONSE_RAW {iterations} {sleep_us}\r".encode()
        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_sram_sleep(self, sleep_start_us: int, sleep_end_us: int, step: int, memory_init: int) -> bytearray:
        command = f"SRAM_SLEEP {sleep_start_us} {sleep_end_us} {step} {memory_init}\r".encode()
        iterations = len(range(sleep_start_us, sleep_end_us, step))

        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def get_sram_deepsleep(self, sleep_start_us: int, sleep_end_us: int, step: int, memory_init: int) -> bytearray:
        command = f"SRAM_DEEPSLEEP {sleep_start_us} {sleep_end_us} {step} {memory_init}\r".encode()
        iterations = len(range(sleep_start_us, sleep_end_us, step))

        self.__send_command(command)
        return self.__read_measurements(iterations, command)

    def execute_command(self, command: Command) -> object:
        method = getattr(self, command.name)
        return method(**command.kwargs)

    def __send_command(self, command: bytes) -> None:
        self.logger.info(f"Command: {command}")
        self.serial.write(command)

    def __wait_ready(self) -> None:
        while line := self.serial.readline():
            if line == b"_____READY_____\r\n":
                self.logger.info("Board ready")
                break

    def __read_measurements(self, iterations: int, command: bytes) -> bytearray:
        data = bytearray()
        i = 0
        while i < iterations:
            line = self.serial.readline()
            if line == b"_____MEASUREMENT_TAG_____\r\n":
                measurement = self.serial.readline().decode().strip()
                self.logger.debug(f"Measurement {i}: '{measurement}'")
                line_bytes = bytearray.fromhex(measurement)
                # assert len(line_bytes) == MEASUREMENT_SIZE, f"Got measurement of size {len(line_bytes)}, " \
                #                                             f"should have been {MEASUREMENT_SIZE}"
                data.extend(line_bytes)
                i += 1

        if self.output_dir is not None:
            self.__print_to_file(command, data)
        self.__wait_ready()
        return data

    def __print_to_file(self, command: bytes, data: bytearray) -> None:
        if len(data) == 0:
            return

        command_string = command.decode().strip().replace(' ', '_').lower()
        file_name = self.output_dir + self.__subdir + self.board_name + "_" + \
                    command_string + self.__file_suffix + ".bin"
        self.logger.info(f"Outputting to: {file_name}")
        with open(file_name, "wb") as file:
            file.write(data)


class ESPCommander:
    stable_time_sec: int = 120
    temp_deviation: float = 0.75

    queues: list
    logger: logging.Logger
    chamber: Chamber
    chamber_idle: bool

    def __init__(self, queues: list, debug: bool):
        self.queues = queues
        self.logger = logging.getLogger()
        self.chamber = Chamber(debug=debug)
        self.chamber_idle = True
        self.stable_time_iterations = self.stable_time_sec // 3  # each loop takes 3 seconds

    def __send_command(self, command: Optional[Command], wait: bool = True) -> None:
        self.logger.info(f"Sending: {command}")
        for queue in self.queues:
            queue.put(command)

        if wait:
            for queue in self.queues:
                queue.join()
        logging.getLogger().info(f"Done: {command}")

    def __temp_in_range(self, temp: float, use_sensor: bool = False) -> bool:
        chamber_temp = self.chamber.read_current_temp()
        sensor_temp = self.chamber.read_sensor_temp()
        self.logger.info(f"Chamber temp: {chamber_temp:.3f}, sensor temp: {sensor_temp:.3f}")
        selected_temp = sensor_temp if use_sensor else chamber_temp
        return abs(temp - selected_temp) < self.temp_deviation

    def __wait_for_temp(self, temp: float) -> None:
        stable = False
        while not stable:
            # this loop needs to complete fully at least once to declare the temp as stable
            for i in range(self.stable_time_iterations):
                time.sleep(1)
                if not self.__temp_in_range(temp):  # temp not stable, break and try the loop again
                    break
            else:
                stable = True

        self.logger.info(f"Temperature of {temp} degrees stabilized")

    def log_temperature(self):
        self.__temp_in_range(0)

    def end_measurement(self) -> None:
        if not self.chamber_idle:
            self.chamber.set_idle()
            self.chamber_idle = True
        self.__send_command(None, False)

    def set_temperature(self, temp: float) -> None:
        if self.chamber_idle:
            self.chamber.clear_idle()
            self.chamber_idle = False

        self.chamber.write_setpoint_temp(temp)
        self.__wait_for_temp(temp)

    def reset_board(self) -> None:
        command = Command("reset_board", {})
        self.__send_command(command)

    def provision(self) -> None:
        command = Command("provision_board", {})
        self.__send_command(command)

    def puf_response(self, iterations: int, sleep_us: int) -> None:
        command = Command("get_puf_response", {"iterations": iterations, "sleep_us": sleep_us})
        self.__send_command(command)

    def puf_response_both(self, iterations: int, sleep_us: int) -> None:
        command = Command("get_puf_response_both", {"iterations": iterations, "sleep_us": sleep_us})
        self.__send_command(command)

    def puf_response_raw(self, iterations: int, sleep_us: int) -> None:
        command = Command("get_puf_response_raw", {"iterations": iterations, "sleep_us": sleep_us})
        self.__send_command(command)

    def pufsleep_response(self, iterations: int, sleep_us: int) -> None:
        command = Command("get_pufsleep_response", {"iterations": iterations, "sleep_us": sleep_us})
        self.__send_command(command)

    def pufsleep_response_raw(self, iterations: int, sleep_us: int) -> None:
        command = Command("get_pufsleep_response_raw", {"iterations": iterations, "sleep_us": sleep_us})
        self.__send_command(command)

    def sram_sleep(self, sleep_start_us: int, sleep_end_us: int, step: int, memory_init: int) -> None:
        kwargs = {
            "sleep_start_us": sleep_start_us,
            "sleep_end_us": sleep_end_us,
            "step": step,
            "memory_init": memory_init,
        }
        command = Command("get_sram_sleep", kwargs)
        self.__send_command(command)

    def sram_deepsleep(self, sleep_start_us: int, sleep_end_us: int, step: int, memory_init: int) -> None:
        kwargs = {
            "sleep_start_us": sleep_start_us,
            "sleep_end_us": sleep_end_us,
            "step": step,
            "memory_init": memory_init,
        }
        command = Command("get_sram_deepsleep", kwargs)
        self.__send_command(command)

    def set_file_suffix(self, file_suffix: str) -> None:
        command = Command("set_file_suffix", {"file_suffix": file_suffix})
        self.__send_command(command)

    def set_output_subdir(self, subdir: str) -> None:
        command = Command("set_output_subdir", {"subdir": subdir})
        self.__send_command(command)


def log_config(logging_queue: Queue) -> None:
    h = logging.handlers.QueueHandler(logging_queue)
    root = logging.getLogger()
    root.handlers.clear()
    root.addHandler(h)
    root.setLevel(LOG_LEVEL)


def commander_process_entry(device: str, output_dir: str, command_queue: JoinableQueue, logging_queue: Queue) -> None:
    log_config(logging_queue)
    board = ESPBoard(device, output_dir=output_dir)
    with board:
        while True:
            command = command_queue.get()
            if command is None:  # sentinel value
                break
            board.execute_command(command)
            command_queue.task_done()
    board.logger.info("Exiting device process")


def create_log_file(log_file):
    parent_dir = os.path.dirname(log_file)
    if parent_dir:
        os.makedirs(parent_dir, exist_ok=True)

    if not os.path.isfile(log_file):
        with open(log_file, "w+") as f:
            f.write(TSV_HEADER)


def logging_process_entry(queue: Queue, loglevel: int, log_file: str) -> None:
    root = logging.getLogger()
    root.handlers.clear()
    root.setLevel(loglevel)
    stream_handler = logging.StreamHandler(sys.stdout)
    stdout_formatter = logging.Formatter(LOG_FORMAT_STDOUT)
    stream_handler.setFormatter(stdout_formatter)
    root.addHandler(stream_handler)
    if log_file is not None:
        create_log_file(log_file)
        file_handler = logging.FileHandler(log_file)
        log_formatter = logging.Formatter(LOG_FORMAT_LOG)
        file_handler.setFormatter(log_formatter)
        root.addHandler(file_handler)

    own_logger = logging.getLogger("logger")
    own_logger.info("Started logging process")

    while True:
        record = queue.get()
        if record is None:
            break
        logger = logging.getLogger(record.name)
        if logger.isEnabledFor(record.levelno):
            logger.handle(record)

    own_logger.info("Exiting logging process")


def get_device_ports() -> list:
    devices = []
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if "CP210" in desc:
            devices.append(port)
    return devices


def start_measurement(commands_function: Callable, output_dir: str = None,
                      log_file: str = None, debug: bool = True) -> None:
    logging_queue = Queue(0)
    logging_process = Process(target=logging_process_entry, args=(logging_queue, LOG_LEVEL, log_file))
    logging_process.start()

    log_config(logging_queue)
    logger = logging.getLogger()

    uart_devices = get_device_ports()
    logger.info(f"Found {len(uart_devices)} devices")

    processes = []
    command_queues = []
    for device in uart_devices:
        command_queue = JoinableQueue(0)
        process = Process(target=commander_process_entry, name=device,
                          args=(device, output_dir, command_queue, logging_queue))
        command_queues.append(command_queue)
        processes.append(process)
        logger.info(f"Starting {device} process")
        process.start()

    if len(uart_devices) > 0:
        commander = ESPCommander(command_queues, debug)
        commands_function(commander)
        commander.end_measurement()

    for process in processes:
        process.join()

    logger.info(f"FINISHED")
    logging_queue.put_nowait(None)
    logging_process.join()
